package com.hsys.tests;

import static com.hsys.utils.ManagerUtils.getHsysPersistUnitEntityManager;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.PropertyConfigurator;

import com.hsys.interfaces.DaoInterface;
import com.hsys.invoices.InvoicesDao;
import com.hsys.invoices.InvoicesTable;

/**@author Rui Lopes*/
public class InvoicesTest {

	static {
		PropertyConfigurator.configure("C:\\tmp\\log4j.properties");
	}
	
	private static final DaoInterface<InvoicesTable> DAO = new InvoicesDao();
	private static final DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss");
	
	private InvoicesTest() {}
	
	public static void main(String ...strin){
		
		EntityManager manager = getHsysPersistUnitEntityManager();
		
		manager.getTransaction().begin();
		
		int action = 4;
		
		try{
			switch(action) {
			
				case 0:
					insertTest(manager);
					break;
					
				case 1:
					System.out.println(selectAllTest(manager));
					break;
					
				case 2:
					System.out.println(selectbyIdTest(1, manager));
					break;
					
				case 3:
					updateTest(manager);
					break;
					
				case 4:
					deleteTest(3, manager);
					break;
			}	
			
		}catch(Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();			
		}finally {
			manager.close();
		}
	}
	
	private static void insertTest(EntityManager manager) throws Exception{
		
		DAO.insert(new InvoicesTable()
				  .setEmployeeNum(1)
				  .setDate(LocalDate.now())
				  .setLasModBy("TestSystem")
				  .setTotalPrice(new BigDecimal(222.23))
				  .setLasModTs(LocalDateTime.now().format(dft))
				  , "", manager);	
	}
	
	private static List<InvoicesTable> selectAllTest(EntityManager manager) throws Exception{
		return DAO.selectAll(manager);
	}
	
	private static InvoicesTable selectbyIdTest(Integer categoryId, EntityManager manager) throws Exception{
		return DAO.selectById(categoryId, "", manager);
	}	
	
	private static InvoicesTable updateTest(EntityManager manager) throws Exception{
		return DAO.update(selectbyIdTest(3, manager).setTotalPrice(new BigDecimal(14))
									, "", manager);
	}
	
	private static void deleteTest(Integer categoryId, EntityManager manager) throws Exception{
		InvoicesTable category = selectbyIdTest(categoryId, manager);
		DAO.delete(category, "", manager);
	}
}