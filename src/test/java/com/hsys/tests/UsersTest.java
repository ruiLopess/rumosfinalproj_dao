package com.hsys.tests;

import static com.hsys.utils.ManagerUtils.getHsysPersistUnitEntityManager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.PropertyConfigurator;

import com.hsys.employees.EmployeesDao;
import com.hsys.interfaces.DaoInterface;
import com.hsys.users.UsersDao;
import com.hsys.users.UsersTable;

/**@author Rui Lopes*/
public class UsersTest {

	static {
		PropertyConfigurator.configure("C:\\tmp\\log4j.properties");
	}
	
	private static final DaoInterface<UsersTable> DAO = new UsersDao();
	private static final DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss");
	
	private UsersTest() {}
	
	public static void main(String ...strin){
		
		EntityManager manager = getHsysPersistUnitEntityManager();
	
		manager.getTransaction().begin();
		
		int action = 4;
		
		try{
			switch(action) {
			
				case 0:
					insertTest(manager);
					break;
					
				case 1:
					System.out.println(selectAllTest(manager));
					break;
					
				case 2:
					System.out.println(selectbyIdTest(3, manager));
					break;
					
				case 3:
					updateTest(manager);
					break;
					
				case 4:
					deleteTest(3, manager);
					break;
			}	
			
		}catch(Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();			
		}finally {
			manager.close();
		}
	}
	
	private static void insertTest(EntityManager manager) throws Exception{
		
		DAO.insert(new UsersTable()
				  .setType(1)
				  .setSalt("adf2edqd")
				  .setEmail("aaa")
				  .setEmployeeNum(1)
				  .setUserName("ocralhes")
				  .setPassword("adf2edqd")
				  .setInsertedDate(LocalDate.now())
				  .setLasModBy("TestSystem")
				  .setLasModTs(LocalDateTime.now().format(dft))
				  , "", manager);	
	}
	
	private static List<UsersTable> selectAllTest(EntityManager manager) throws Exception{
		return DAO.selectAll(manager);
	}
	
	private static UsersTable selectbyIdTest(Integer categoryId, EntityManager manager) throws Exception{
		return DAO.selectById(categoryId, "", manager);
	}	
	
	private static UsersTable updateTest(EntityManager manager) throws Exception{
		return DAO.update(selectbyIdTest(3, manager).setUserName("JorgeJarbasJesus"), "", manager);
	}
	
	private static void deleteTest(Integer categoryId, EntityManager manager) throws Exception{
		UsersTable category = selectbyIdTest(categoryId, manager);
		DAO.delete(category, "", manager);
	}
}