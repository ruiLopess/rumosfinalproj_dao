package com.hsys.tests;

import static com.hsys.utils.ManagerUtils.getHsysPersistUnitEntityManager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.PropertyConfigurator;

import com.hsys.categories.CategoriesTable;
import com.hsys.employees.EmployeesDao;
import com.hsys.employees.EmployeesTable;
import com.hsys.interfaces.DaoInterface;

/**@author Rui Lopes*/
public class EmployeesTest {

	static {
		PropertyConfigurator.configure("C:\\tmp\\log4j.properties");
	}
	
	private static final DaoInterface<EmployeesTable> DAO = new EmployeesDao();
	private static final DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss");
	
	private EmployeesTest() {}
	
	public static void main(String ...strin){
		
		EntityManager manager = getHsysPersistUnitEntityManager();
		
		manager.getTransaction().begin();
		
		int action = 0;
		
		try{
			switch(action) {
			
				case 0:
					insertTest(manager);
					break;
					
				case 1:
					System.out.println(selectAllTest(manager));
					break;
					
				case 2:
					System.out.println(selectbyIdTest(1, manager));
					break;
					
				case 3:
					updateTest(manager);
					break;
					
				case 4:
					deleteTest(4, manager);
					break;
			}	
			
		}catch(Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
		}finally {
			manager.close();
		}
	}
	
	private static void insertTest(EntityManager manager) throws Exception{
		
		DAO.insert(new EmployeesTable()
				  .setSex("M")
				  .setFirstName("dfdwwwwsa")
				  .setLastName("JArfdssfbas")
				  .setBirthdate(LocalDate.now())
				  .setFinancialNumber(12342134)
				  .setLasModBy("TestSystem")
				  .setLasModTs(LocalDateTime.now().format(dft))
				  , "", manager);	
	}
	
	private static List<EmployeesTable> selectAllTest(EntityManager manager) throws Exception{
		return DAO.selectAll(manager);
	}
	
	private static EmployeesTable selectbyIdTest(Integer categoryId, EntityManager manager) throws Exception{
		return DAO.selectById(categoryId, "", manager);
	}	
	
	private static EmployeesTable updateTest(EntityManager manager) throws Exception{
		return DAO.update(selectbyIdTest(4, manager).setFirstName("JorgeJarbasJesus")
									, "", manager);
	}
	
	private static void deleteTest(Integer categoryId, EntityManager manager) throws Exception{
		EmployeesTable category = selectbyIdTest(categoryId, manager);
		DAO.delete(category, "", manager);
	}
}