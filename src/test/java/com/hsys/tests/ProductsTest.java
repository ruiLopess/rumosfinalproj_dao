package com.hsys.tests;

import static com.hsys.utils.ManagerUtils.getHsysPersistUnitEntityManager;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.PropertyConfigurator;

import com.hsys.interfaces.DaoInterface;
import com.hsys.products.ProductsDao;
import com.hsys.products.ProductsTable;

/**@author Rui Lopes*/
public class ProductsTest {

	static {
		PropertyConfigurator.configure("C:\\tmp\\log4j.properties");
	}
	
	private static final DaoInterface<ProductsTable> DAO = new ProductsDao();
	private static final DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss");
	
	private ProductsTest() {}
	
	public static void main(String ...strin){
		
		EntityManager manager = getHsysPersistUnitEntityManager();
		
		manager.getTransaction().begin();
		
		int action = 4;
		
		try{
			switch(action) {
			
				case 0:
					insertTest(manager);
					break;
					
				case 1:
					System.out.println(selectAllTest(manager));
					break;
					
				case 2:
					System.out.println(selectbyIdTest(5, manager));
					break;
					
				case 3:
					updateTest(manager);
					break;
					
				case 4:
					deleteTest(5, manager);
					break;
			}	
			
		}catch(Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();			
		}finally {
			manager.close();
		}
	}
	
	private static void insertTest(EntityManager manager) throws Exception{
		
		DAO.insert(new ProductsTable()
				  .setName("prod4")
				  .setDesc("cenas do jarbas")
				  .setInsertedBy("TestSystem")
				  .setPrice(new BigDecimal(9999.2))
				  .setCategoryId(1)
				  .setSubCategoryId(1)
				  .setInsertedDate(LocalDate.now())
				  .setLasModBy("TestSystem")
				  .setLasModTs(LocalDateTime.now().format(dft))
				  , "", manager);	
	}
	
	private static List<ProductsTable> selectAllTest(EntityManager manager) throws Exception{
		return DAO.selectAll(manager);
	}
	
	private static ProductsTable selectbyIdTest(Integer categoryId, EntityManager manager) throws Exception{
		return DAO.selectById(categoryId, "", manager);
	}	
	
	private static ProductsTable updateTest(EntityManager manager) throws Exception{
		return DAO.update(selectbyIdTest(2, manager).setName("prodUpdated")
									, "", manager);
	}
	
	private static void deleteTest(Integer categoryId, EntityManager manager) throws Exception{
		ProductsTable category = selectbyIdTest(categoryId, manager);
		DAO.delete(category, "", manager);
	}
}