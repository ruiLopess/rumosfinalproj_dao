package com.hsys.tests;

import static com.hsys.utils.ManagerUtils.getHsysPersistUnitEntityManager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.PropertyConfigurator;

import com.hsys.interfaces.DaoInterface;
import com.hsys.subcategories.SubCategoriesDao;
import com.hsys.subcategories.SubCategoriesTable;

/**@author Rui Lopes*/
public class SubCategoriesTest {

	static {
		PropertyConfigurator.configure("C:\\tmp\\log4j.properties");
	}
	
	private static final DaoInterface<SubCategoriesTable> DAO = new SubCategoriesDao();
	private static final DateTimeFormatter dft = DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss");
	
	private SubCategoriesTest() {}
	
	public static void main(String ...strin){
		
		EntityManager manager = getHsysPersistUnitEntityManager();
		
		manager.getTransaction().begin();
		
		int action = 4;
		
		try{
			switch(action) {
			
				case 0:
					insertTest(manager);
					break;
					
				case 1:
					System.out.println(selectAllTest(manager));
					break;
					
				case 2:
					System.out.println(selectbyIdTest(7, manager));
					break;
					
				case 3:
					updateTest(manager);
					break;
					
				case 4:
					deleteTest(7, manager);
					break;
			}	
			
		}catch(Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();			
		}finally {
			manager.close();
		}
	}
	
	private static void insertTest(EntityManager manager) throws Exception{
		
		DAO.insert(new SubCategoriesTable()
				  .setCategoryId(1)
				  .setInsertedBy("TestSystem")
				  .setSubCategoryName("Cat4")
				  .setInsertedDate(LocalDate.now())				  
				  .setLasModBy("TestSystem")
				  .setLasModTs(LocalDateTime.now().format(dft))
				  , "", manager);	
	}
	
	private static List<SubCategoriesTable> selectAllTest(EntityManager manager) throws Exception{
		return DAO.selectAll(manager);
	}
	
	private static SubCategoriesTable selectbyIdTest(Integer categoryId, EntityManager manager) throws Exception{
		return DAO.selectById(categoryId, "", manager);
	}	
	
	private static SubCategoriesTable updateTest(EntityManager manager) throws Exception{
		return DAO.update(selectbyIdTest(7, manager).setSubCategoryName("JorgeJarbasJesus")
									, "", manager);
	}
	
	private static void deleteTest(Integer categoryId, EntityManager manager) throws Exception{
		SubCategoriesTable category = selectbyIdTest(categoryId, manager);
		DAO.delete(category, "", manager);
	}
}