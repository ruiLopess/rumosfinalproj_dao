package com.hsys.invoices;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Rui Lopes*/
@Entity
@Table(name = "invoices")
public class InvoicesTable implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private LocalDate date;
	private String lasModBy;
	private String lasModTs;
	private Integer invoiceId;
	private Integer employeeNum;
	private BigDecimal totalPrice;
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("[date=").append(date).append(", lasModBy=").append(lasModBy).append(", lasModTs=")
				.append(lasModTs).append(", invoiceId=").append(invoiceId).append(", employeeNum=").append(employeeNum)
				.append(", totalPrice=").append(totalPrice).append("]").toString();
	}
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "InvoiceId", nullable = false, unique = true)
	public Integer getInvoiceId() {
		return invoiceId;
	}
	@Column(name = "Date", nullable = false)
	public LocalDate getDate() {
		return date;
	}
	@Column(name = "LastModBy", length = 50, nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	@Column(name = "EmployeeNum", nullable = false)
	public Integer getEmployeeNum() {
		return employeeNum;
	}
	@Column(name = "TotalPrice", nullable = false)
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public InvoicesTable setDate(LocalDate date) {
		this.date = date;
		return this;
	}
	public InvoicesTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public InvoicesTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}
	public InvoicesTable setEmployeeNum(Integer employeeNum) {
		this.employeeNum = employeeNum;
		return this;
	}
	public InvoicesTable setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
		return this;
	}
	public InvoicesTable setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
		return this;
	}
}