package com.hsys.invoices;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.employees.EmployeesTable;
import com.hsys.interfaces.DaoInterface;

/**@author Rui Lopes*/
public class InvoicesDao implements DaoInterface <InvoicesTable>{
	
	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = InvoicesDao.class.getSimpleName();
	
	@Override
	public List<InvoicesTable> genericSelect(EntityManager manager, String query) throws Exception {
		return null;
	} 
	
	@Override
	public List<InvoicesTable> selectAll(EntityManager manager) throws Exception {

		String sQuery = "SELECT invoices FROM InvoicesTable invoices";
		return manager.createQuery(sQuery, InvoicesTable.class)
					  .getResultList();
	}
	
	@Override
	public InvoicesTable selectById(Integer invoiceId, String uuid, EntityManager manager) throws Exception {
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Searching for invoice: " + invoiceId);
		return manager.find(InvoicesTable.class, invoiceId);
	}
	
	@Override
	public void insert(InvoicesTable invoice, String uuid, EntityManager manager) throws Exception {
		
		manager.persist(invoice);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Invoice inserted: " + invoice);
	}
	
	@Override
	public InvoicesTable update(InvoicesTable invoice, String uuid, EntityManager manager) throws Exception {

		InvoicesTable olcInvoice = selectById(invoice.getInvoiceId(), uuid, manager);
		logger.trace(uuid + " [" + CLASS_NAME + "] Invoice on table: " + olcInvoice);
		
		olcInvoice = invoice;
		
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Invoice after update: " + olcInvoice);
		
		return olcInvoice;
	}
	
	@Override
	public void delete(InvoicesTable invoice, String uuid, EntityManager manager) throws Exception {
		manager.remove(invoice);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted invoice: " + invoice);
	}	
}