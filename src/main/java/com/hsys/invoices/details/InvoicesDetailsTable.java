package com.hsys.invoices.details;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Rui Lopes*/
@Entity
@Table(name = "invoices_details")                  
public class InvoicesDetailsTable implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer seqNum;
	private Integer invoiceId;
	private String lasModBy;
	private String lasModTs;
	private BigDecimal price;
	private Integer productId;	
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("[seqNum=").append(seqNum).append(", invoiceId=").append(invoiceId).append(", lasModBy=")
				.append(lasModBy).append(", lasModTs=").append(lasModTs).append(", price=").append(price)
				.append(", productId=").append(productId).append("]").toString();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "seqNum", nullable = false, unique = true)
	public Integer getSeqNum() {
		return seqNum;
	}
	@Column(name = "InvoiceId", nullable = false)
	public Integer getInvoiceId() {
		return invoiceId;
	}
	@Column(name = "LastModBy", nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	@Column(name = "Price", nullable = false)
	public BigDecimal getPrice() {
		return price;
	}
	@Column(name = "ProductId", nullable = false)
	public Integer getProductId() {
		return productId;
	}
	public InvoicesDetailsTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public InvoicesDetailsTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}
	public InvoicesDetailsTable setPrice(BigDecimal price) {
		this.price = price;
		return this;
	}
	public InvoicesDetailsTable setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}
	public InvoicesDetailsTable setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
		return this;
	}
	public InvoicesDetailsTable setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
		return this;
	}
}