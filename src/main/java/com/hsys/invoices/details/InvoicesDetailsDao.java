package com.hsys.invoices.details;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.categories.CategoriesTable;
import com.hsys.interfaces.DaoInterface;
import com.hsys.invoices.InvoicesTable;

/**@author Rui Lopes*/
public class InvoicesDetailsDao implements DaoInterface <InvoicesDetailsTable>{

	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = InvoicesDetailsDao.class.getSimpleName();
	
	/**
	 * SELECT details FROM InvoicesDetailsTable details
	 * WHERE invoiceId=:idTeste
	 */
	@Override
	public List<InvoicesDetailsTable> genericSelect(EntityManager manager, String query) throws Exception {
		
		String sQuery = "SELECT details FROM InvoicesDetailsTable details"
				  + " WHERE invoiceId=:idTeste";
		
		return manager.createQuery(sQuery, InvoicesDetailsTable.class)
					  .setParameter("idTeste", query)
					  .getResultList();
	}
	
	@Override
	public List<InvoicesDetailsTable> selectAll(EntityManager manager) throws Exception{
		
		String sQuery = "SELECT details FROM InvoicesDetailsTable details";
		return manager.createQuery(sQuery, InvoicesDetailsTable.class)
					  .getResultList();
	}
	
	/**
	 * not used here
	 */
	@Override
	public InvoicesDetailsTable selectById(Integer invoiceId, String uuid, EntityManager manager) throws Exception{
		return null;
	}	
	
	@Override
	public void insert(InvoicesDetailsTable invoice, String uuid, EntityManager manager) throws Exception{
		
		manager.persist(invoice);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Invoice details inserted: " + invoice);
	}
	
	@Override
	public InvoicesDetailsTable update(InvoicesDetailsTable invoice, String uuid, EntityManager manager) throws Exception{
		
		InvoicesDetailsTable oldInvoice = selectById(invoice.getInvoiceId(), uuid, manager);
		logger.trace(uuid + " [" + CLASS_NAME + "] Invoice on table: " + oldInvoice);
		
		oldInvoice = invoice;
		manager.getTransaction().commit();
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Invoice after update: " + oldInvoice);
		
		manager.close();
		
		return oldInvoice;
	}
	
	@Override
	public void delete(InvoicesDetailsTable invoice, String uuid, EntityManager manager) throws Exception{
		
		manager.remove(invoice);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted invoice: " + invoice);
	}
}