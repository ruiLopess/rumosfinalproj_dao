package com.hsys.categories;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.interfaces.DaoInterface;

/**@author Rui Lopes*/
public class CategoriesDao implements DaoInterface <CategoriesTable>{
	
	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = CategoriesDao.class.getSimpleName();

	@Override
	public List<CategoriesTable> genericSelect(EntityManager manager, String query) throws Exception {
		return null;
	}
	
	@Override
	public List<CategoriesTable> selectAll(EntityManager manager) throws Exception {
		
		String sQuery = "SELECT categories FROM CategoriesTable categories";
		return manager.createQuery(sQuery, CategoriesTable.class)
					  .getResultList();
	}
	
	@Override
	public CategoriesTable selectById(Integer categoryId, String uuid, EntityManager manager) throws Exception {
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Searching for category: " + categoryId);
		return manager.find(CategoriesTable.class, categoryId);
	}

	@Override
	public void insert(CategoriesTable category, String uuid, EntityManager manager) throws Exception {
		
		manager.persist(category);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Category inserted: " + category);
	}
	
	@Override
	public CategoriesTable update(CategoriesTable category, String uuid, EntityManager manager) throws Exception {
					
		CategoriesTable	oldCategory = selectById(category.getCotegoryId(), uuid, manager);
		logger.trace(uuid + " [" + CLASS_NAME + "] Categories on table: " + oldCategory);
		
		oldCategory = category;
		
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Category after update: " + oldCategory);
		
		return oldCategory;
	}

	@Override
	public void delete(CategoriesTable category, String uuid, EntityManager manager) throws Exception {
		manager.remove(category);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted category: " + category);
	}
}