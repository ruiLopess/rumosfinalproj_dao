package com.hsys.categories;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Rui Lopes*/
@Entity
@Table(name = "categories")
public class CategoriesTable implements Serializable{

	private static final long serialVersionUID = 1L;

	private String lasModBy;
	private String lasModTs;
	private Integer cotegoryId;
	private String categoryName;
	private String insertedBy;
	private LocalDate insertedDate;
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("[lasModBy=").append(lasModBy).append(", lasModTs=").append(lasModTs).append(", cotegoryId=")
				.append(cotegoryId).append(", categoryName=").append(categoryName).append(", insertedBy=")
				.append(insertedBy).append(", insertedDate=").append(insertedDate).append("]").toString();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CategoryId", nullable = false, unique = true)
	public Integer getCotegoryId() {
		return cotegoryId;
	}
	@Column(name = "LastModBy", length = 50, nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	@Column(name = "CategoryName", length = 25, nullable = false)
	public String getCategoryName() {
		return categoryName;
	}
	@Column(name = "InsertedBy", length = 50, nullable = false, unique = true)
	public String getInsertedBy() {
		return insertedBy;
	}
	@Column(name = "InsertedDate", nullable = false)
	public LocalDate getInsertedDate() {
		return insertedDate;
	}
	public CategoriesTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public CategoriesTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}
	public CategoriesTable setCotegoryId(Integer cotegoryId) {
		this.cotegoryId = cotegoryId;
		return this;
	}
	public CategoriesTable setCategoryName(String categoryName) {
		this.categoryName = categoryName;
		return this;
	}
	public CategoriesTable setInsertedBy(String insertedBy) {
		this.insertedBy = insertedBy;
		return this;
	}
	public CategoriesTable setInsertedDate(LocalDate insertedDate) {
		this.insertedDate = insertedDate;
		return this;
	}
}