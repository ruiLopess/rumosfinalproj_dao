package com.hsys.interfaces;

import java.util.List;

import javax.persistence.EntityManager;

/**@author Rui Lopes*/
public interface DaoInterface<T> {

	List<T> selectAll(EntityManager manager) throws Exception;

	List<T> genericSelect(EntityManager manager, String query) throws Exception;
	
	/**
		<b>In the implementation InvoicesDetailsDao this method does not do anything
		<p>Use the genericSelect instead
		<b>
	 */
	T selectById(Integer id, String uuid, EntityManager manager) throws Exception;
	
	void insert(T table, String uuid, EntityManager manager) throws Exception;
	
	T update(T table, String uuid, EntityManager manager) throws Exception;
	
	void delete(T table, String uuid, EntityManager manager) throws Exception;
}