package com.hsys.users;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.interfaces.DaoInterface;
import com.hsys.subcategories.SubCategoriesTable;

/**@author Rui Lopes*/
public class UsersDao implements DaoInterface <UsersTable>{

	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = UsersDao.class.getSimpleName();
	
	@Override
	public List<UsersTable> genericSelect(EntityManager manager, String query) throws Exception {
		return null;
	}
	
	@Override
	public List<UsersTable> selectAll(EntityManager manager) throws Exception{

		String sQuery = "SELECT users FROM UsersTable users";
		return manager.createQuery(sQuery, UsersTable.class)
					  .getResultList();
	}
	
	@Override
	public UsersTable selectById(Integer employeeNum, String uuid, EntityManager manager) throws Exception{
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Searching for user (employeeNum): " + employeeNum);
		return manager.find(UsersTable.class, employeeNum);
	}	
	
	@Override
	public void insert(UsersTable user, String uuid, EntityManager manager) throws Exception{
		
		manager.persist(user);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] User inserted: " + user);
	}
	
	@Override
	public UsersTable update(UsersTable user, String uuid, EntityManager manager) throws Exception{
		
		UsersTable oldUser = selectById(user.getEmployeeNum(), uuid, manager);
		logger.trace("[" + CLASS_NAME + "] User on table: " + oldUser);
		
		oldUser = user;
		
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] User after update: " + oldUser);
		
		return oldUser;
	}
	
	@Override
	public void delete(UsersTable user, String uuid, EntityManager manager) throws Exception{

		manager.remove(user);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted user: " + user);
	}
}