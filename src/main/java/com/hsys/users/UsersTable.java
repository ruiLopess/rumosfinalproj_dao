package com.hsys.users;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hsys.employees.EmployeesTable;

/**@author Rui Lopes*/
@Entity
@Table(name = "users")
public class UsersTable implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String salt;
	private String email;
	private Integer type;
	private String userName;
	private String password;
	private String lasModBy;
	private String lasModTs;	
	private Integer employeeNum;
	private LocalDate insertedDate;
	
	@Override
	public String toString() {
		return new StringBuilder().append("[type=").append(type).append(", salt=").append(salt)
				  .append(", email=").append(email).append(", userName=").append(userName).append(", password=")
				  .append(password).append(", lasModBy=").append(lasModBy).append(", employeeNum=")
				  .append(employeeNum).append(", insertedDate=").append(insertedDate).append(", lasModTs=")
				  .append(lasModTs).append("]").toString();
	}
	
	@Column(name = "Username", length = 50, nullable = false, unique = true)
	public String getUserName() {
		return userName;
	}
	@Id
	@Column(name = "EmployeeNum", nullable = false, unique = true)
	public Integer getEmployeeNum() {
		return employeeNum;
	}
	@Column(name = "Type", nullable = false)
	public Integer getType() {
		return type;
	}
	@Column(name = "Email", length = 100, nullable = false, unique = true)
	public String getEmail() {
		return email;
	}
	@Column(name = "Password", length = 100, nullable = false)
	public String getPassword() {
		return password;
	}
	@Column(name = "Salt", length = 50, nullable = false)
	public String getSalt() {
		return salt;
	}
	@Column(name = "InsertedDate", nullable = false)
	public LocalDate getInsertedDate() {
		return insertedDate;
	}
	@Column(name = "LastModBy", length = 50, nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	public UsersTable setType(Integer type) {
		this.type = type;
		return this;
	}
	public UsersTable setSalt(String salt) {
		this.salt = salt;
		return this;
	}
	public UsersTable setEmail(String email) {
		this.email = email;
		return this;
	}
	public UsersTable setUserName(String userName) {
		this.userName = userName;
		return this;
	}
	public UsersTable setPassword(String password) {
		this.password = password;
		return this;
	}
	public UsersTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public UsersTable setEmployeeNum(Integer employeeNum) {
		this.employeeNum = employeeNum;
		return this;
	}
	public UsersTable setInsertedDate(LocalDate insertedDate) {
		this.insertedDate = insertedDate;
		return this;
	}
	public UsersTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}	
}