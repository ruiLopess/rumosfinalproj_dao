package com.hsys.products;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Rui Lopes*/
@Entity
@Table(name = "products")
public class ProductsTable implements Serializable{

	private static final long serialVersionUID = 1L;

	private String name;
	private String desc;
	private String lasModBy;
	private String lasModTs;
	private String insertedBy;
	private BigDecimal price;
	private Integer productId;
	private Integer categoryId;
	private Integer subCategoryId;
	private LocalDate insertedDate;
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("[desc=").append(desc).append(", lasModBy=").append(lasModBy).append(", lasModTs=")
				.append(lasModTs).append(", useraname=").append(insertedBy).append(", price=").append(price)
				.append(", productId=").append(productId).append(", categoryId=").append(categoryId)
				.append(", subCategoryId=").append(subCategoryId).append(", insertedDate=").append(insertedDate)
				.append(", name=").append(name).append("]").toString();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ProductId", nullable = false, unique = true)
	public Integer getProductId() {
		return productId;
	}
	@Column(name = "ProductName", length = 50, nullable = false)
	public String getName() {
		return name;
	}
	@Column(name = "ProdDesc", length = 350, nullable = false)
	public String getDesc() {
		return desc;
	}
	@Column(name = "InsertedBy", length = 50, nullable = false)
	public String getInsertedBy() {
		return insertedBy;
	}
	@Column(name = "Price", nullable = false)
	public BigDecimal getPrice() {
		return price;
	}
	@Column(name = "CategoryId", nullable = false)
	public Integer getCategoryId() {
		return categoryId;
	}
	@Column(name = "SubCategoryId", nullable = false)
	public Integer getSubCategoryId() {
		return subCategoryId;
	}
	@Column(name = "InsertedDate", nullable = false)
	public LocalDate getInsertedDate() {
		return insertedDate;
	}
	@Column(name = "LastModBy", length = 50, nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	public ProductsTable setName(String name) {
		this.name = name;
		return this;
	}
	public ProductsTable setDesc(String desc) {
		this.desc = desc;
		return this;
	}
	public ProductsTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public ProductsTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}
	public ProductsTable setInsertedBy(String insertedBy) {
		this.insertedBy = insertedBy;
		return this;
	}
	public ProductsTable setPrice(BigDecimal price) {
		this.price = price;
		return this;
	}
	public ProductsTable setProductId(Integer productId) {
		this.productId = productId;
		return this;
	}
	public ProductsTable setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
		return this;
	}
	public ProductsTable setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
		return this;
	}
	public ProductsTable setInsertedDate(LocalDate insertedDate) {
		this.insertedDate = insertedDate;
		return this;
	}
}