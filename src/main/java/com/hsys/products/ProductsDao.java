package com.hsys.products;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.categories.CategoriesTable;
import com.hsys.interfaces.DaoInterface;
import com.hsys.invoices.details.InvoicesDetailsTable;

/**@author Rui Lopes*/
public class ProductsDao implements DaoInterface <ProductsTable>{

	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = ProductsDao.class.getSimpleName();

	@Override
	public List<ProductsTable> genericSelect(EntityManager manager, String query) throws Exception {
		return null;
	} 
	
	@Override	
	public List<ProductsTable> selectAll(EntityManager manager) throws Exception{
		
		String sQuery = "SELECT products FROM ProductsTable products";
		return manager.createQuery(sQuery, ProductsTable.class)
					  .getResultList();
	}
	
	@Override
	public ProductsTable selectById(Integer productId, String uuid, EntityManager manager)  throws Exception{
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Searching for product: " + productId);
		return manager.find(ProductsTable.class, productId);
	}	
	
	@Override
	public void insert(ProductsTable product, String uuid, EntityManager manager) {
	
		manager.persist(product);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Product inserted: " + product);
	}
	
	@Override
	public ProductsTable update(ProductsTable product, String uuid, EntityManager manager)  throws Exception{	
		
		ProductsTable oldProduct = selectById(product.getProductId(), uuid, manager);
		logger.trace(uuid + " [" + CLASS_NAME + "] Product on table: " + oldProduct);
		
		oldProduct = product;
		
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Product after update: " + oldProduct);
		
		return oldProduct;
	}
	
	@Override
	public void delete(ProductsTable product, String uuid, EntityManager manager)  throws Exception{
		manager.remove(product);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted product: " + product);
	}
}