package com.hsys.userstype;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Rui Lopes*/
@Entity
@Table(name = "users_type")
public class UsersTypeTable implements Serializable{

	private static final long serialVersionUID = 1L;

	private String desc;
	private Integer typeId;
	private String lasModBy;
	private String lasModTs;
	private String insertedBy;
	private LocalDate insertedDate;

	@Override
	public String toString() {
		return new StringBuilder()
				.append("[desc=").append(desc).append(", typeId=").append(typeId).append(", lasModBy=").append(lasModBy)
				.append(", lasModTs=").append(lasModTs).append(", insertedBy=").append(insertedBy)
				.append(", insertedDate=").append(insertedDate).append("]").toString();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TypeId", nullable = false, unique = true)
	public Integer getTypeId() {
		return typeId;
	}
	@Column(name = "UserTypeDesc", length = 25, nullable = false)
	public String getDesc() {
		return desc;
	}
	@Column(name = "InsertedBy", length = 50, nullable = false, unique = true)
	public String getInsertedBy() {
		return insertedBy;
	}
	@Column(name = "InsertedDate", nullable = false)
	public LocalDate getInsertedDate() {
		return insertedDate;
	}
	@Column(name = "LastModBy", length = 50, nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	public UsersTypeTable setDesc(String desc) {
		this.desc = desc;
		return this;
	}
	public UsersTypeTable setTypeId(Integer typeId) {
		this.typeId = typeId;
		return this;
	}
	public UsersTypeTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public UsersTypeTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}
	public UsersTypeTable setInsertedBy(String insertedBy) {
		this.insertedBy = insertedBy;
		return this;
	}
	public UsersTypeTable setInsertedDate(LocalDate insertedDate) {
		this.insertedDate = insertedDate;
		return this;
	}
}