package com.hsys.userstype;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.categories.CategoriesTable;
import com.hsys.interfaces.DaoInterface;
import com.hsys.users.UsersTable;

/**@author Rui Lopes*/
public class UsersTypeDao implements DaoInterface <UsersTypeTable>{

	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = UsersTypeDao.class.getSimpleName();

	@Override
	public List<UsersTypeTable> genericSelect(EntityManager manager, String query) throws Exception {
		return null;
	}
	
	@Override
	public List<UsersTypeTable> selectAll(EntityManager manager) throws Exception{
		
		String sQuery = "SELECT usersType FROM UsersTypeTable usersType";
		return manager.createQuery(sQuery, UsersTypeTable.class)
					  						 .getResultList();
	}
	
	@Override
	public UsersTypeTable selectById(Integer userTypeId, String uuid, EntityManager manager) throws Exception {
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Searching for User Type: " + userTypeId);
		return manager.find(UsersTypeTable.class, userTypeId);
	}	
	
	@Override
	public void insert(UsersTypeTable userTypeId, String uuid, EntityManager manager) {

		manager.persist(userTypeId);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] User Type inserted: " + userTypeId);
	}
	
	@Override
	public UsersTypeTable update(UsersTypeTable userType, String uuid, EntityManager manager) throws Exception {
				
		UsersTypeTable oldUserType = selectById(userType.getTypeId(), uuid, manager);
		logger.trace(uuid + " [" + CLASS_NAME + "] User Type on table: " + oldUserType);
		
		oldUserType = userType;
		
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] User Type after update: " + oldUserType);
		
		return oldUserType;
	}
	
	@Override
	public void delete(UsersTypeTable userTypeId, String uuid, EntityManager manager) throws Exception {
		
		manager.remove(userTypeId);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted User Type: " + userTypeId);
	}
}