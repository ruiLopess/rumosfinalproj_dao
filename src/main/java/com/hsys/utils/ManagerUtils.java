package com.hsys.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class ManagerUtils {
	
	private ManagerUtils(){}
	
	public static EntityManager getHsysPersistUnitEntityManager() {
		return Persistence.createEntityManagerFactory("hsysPersistUnit")
						  .createEntityManager();
	}
	
	public static EntityTransaction getHsysPersistUnitEntityTransaction() {
		return getHsysPersistUnitEntityManager().getTransaction();
	}
}