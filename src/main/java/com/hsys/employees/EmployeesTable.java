package com.hsys.employees;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Rui Lopes*/
@Entity
@Table(name = "employees")
public class EmployeesTable implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String sex;
	private String lastName;
	private String lasModBy;
	private String lasModTs;
	private String firstName;
	private Integer employeeNum;
	private LocalDate birthdate;
	private Integer financialNumber;
	
	@Override
	public String toString() {
		return new StringBuilder()
				  .append("[sex=").append(sex).append(", lastName=").append(lastName).append(", lasModBy=")
				  .append(lasModBy).append(", lasModTs=").append(lasModTs).append(", employeeNum=").append(employeeNum)
				  .append(", firstName=").append(firstName).append(", birthdate=").append(birthdate)
				  .append(", financialNumber=").append(financialNumber).append("]").toString();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EmployeeNum", nullable = false, unique = true)
	public Integer getEmployeeNum() {
		return employeeNum;
	}
	@Column(name = "Sex", length = 1, nullable = false)
	public String getSex() {
		return sex;
	}
	@Column(name = "LastName", length = 25, nullable = false)
	public String getLastName() {
		return lastName;
	}
	@Column(name = "LastModBy", length = 50, nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	@Column(name = "FirstName", length = 25, nullable = false)
	public String getFirstName() {
		return firstName;
	}
	@Column(name = "Birthdate", length = 25, nullable = false)
	public LocalDate getBirthdate() {
		return birthdate;
	}
	@Column(name = "FinancialNumber", nullable = false)
	public Integer getFinancialNumber() {
		return financialNumber;
	}
	public EmployeesTable setSex(String sex) {
		this.sex = sex;
		return this;
	}
	public EmployeesTable setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
	public EmployeesTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public EmployeesTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}
	public EmployeesTable setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}
	public EmployeesTable setEmployeeNum(Integer employeeNum) {
		this.employeeNum = employeeNum;
		return this;
	}
	public EmployeesTable setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
		return this;
	}
	public EmployeesTable setFinancialNumber(Integer financialNumber) {
		this.financialNumber = financialNumber;
		return this;
	}
}