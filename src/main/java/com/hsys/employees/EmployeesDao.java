package com.hsys.employees;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.categories.CategoriesTable;
import com.hsys.interfaces.DaoInterface;

/**@author Rui Lopes*/
public class EmployeesDao implements DaoInterface <EmployeesTable>{
	
	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = EmployeesDao.class.getSimpleName();
	
	@Override
	public List<EmployeesTable> genericSelect(EntityManager manager, String query) throws Exception {
		return null;
	}
	
	@Override
	public List<EmployeesTable> selectAll(EntityManager manager) throws Exception {
		
		String sQuery = "SELECT employees FROM EmployeesTable employees";
		return manager.createQuery(sQuery, EmployeesTable.class)
					  .getResultList();
	}
	
	
	@Override
	public EmployeesTable selectById(Integer employee, String uuid, EntityManager manager) throws Exception {
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Searching for employee: " + employee);
		return manager.find(EmployeesTable.class, employee);
	}	
	
	@Override
	public void insert(EmployeesTable employees, String uuid, EntityManager manager) throws Exception {

		manager.persist(employees);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Employee inserted: " + employees);
	}
	
	@Override
	public EmployeesTable update(EmployeesTable employee, String uuid, EntityManager manager) throws Exception {
		
		EmployeesTable oldEmployee = selectById(employee.getEmployeeNum(), uuid, manager);
		logger.trace("[" + CLASS_NAME + "] Employee on table: " + oldEmployee);
		
		oldEmployee = employee;
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Employee after update: " + oldEmployee);
		
		return oldEmployee;
	}
	
	@Override
	public void delete(EmployeesTable employee, String uuid, EntityManager manager) throws Exception {

		manager.remove(employee);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted employee: " + employee);
	}
}