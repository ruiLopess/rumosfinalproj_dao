package com.hsys.subcategories;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.hsys.interfaces.DaoInterface;
import com.hsys.products.ProductsTable;

/**@author Rui Lopes*/
public class SubCategoriesDao implements DaoInterface <SubCategoriesTable>{

	private static Logger logger = Logger.getLogger("DaoLogger");
	private static final String CLASS_NAME = SubCategoriesDao.class.getSimpleName();

	@Override
	public List<SubCategoriesTable> genericSelect(EntityManager manager, String query) throws Exception {
		return null;
	}
	
	@Override
	public List<SubCategoriesTable> selectAll(EntityManager manager) throws Exception{
		
		String sQuery = "SELECT products FROM SubCategoriesTable products";
		return manager.createQuery(sQuery, SubCategoriesTable.class)
					  .getResultList();
	}
	
	@Override
	public SubCategoriesTable selectById(Integer subCategoryId, String uuid, EntityManager manager)  throws Exception{
		
		logger.trace(uuid + " [" + CLASS_NAME + "] Searching for subCategory: " + subCategoryId);
		return manager.find(SubCategoriesTable.class, subCategoryId);
	}	
	
	@Override
	public void insert(SubCategoriesTable subCategory, String uuid, EntityManager manager)  throws Exception{

		manager.persist(subCategory);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] SubCategory inserted: " + subCategory);
	}
	
	@Override
	public SubCategoriesTable update(SubCategoriesTable subCategory, String uuid, EntityManager manager)  throws Exception{
		
		SubCategoriesTable oldSubCategory = selectById(subCategory.getSubCategoryId(), uuid, manager);
		logger.trace(uuid + " [" + CLASS_NAME + "] SubCategory on table: " + oldSubCategory);
		
		oldSubCategory = subCategory;
		
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] SubCategory after update: " + oldSubCategory);
		
		return oldSubCategory;
	}
	
	@Override
	public void delete(SubCategoriesTable subCategory, String uuid, EntityManager manager)  throws Exception{

		manager.remove(subCategory);
		manager.getTransaction().commit();
		logger.trace(uuid + " [" + CLASS_NAME + "] Deleted subCategory: " + subCategory);
	}
}