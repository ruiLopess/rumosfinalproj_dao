package com.hsys.subcategories;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Rui Lopes*/
@Entity
@Table(name = "sub_categories")
public class SubCategoriesTable implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String lasModBy;
	private String lasModTs;
	private String insertedBy;
	private Integer categoryId;	
	private Integer subCategoryId;
	private String subCategoryName;	
	private LocalDate insertedDate;
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("[lasModBy=").append(lasModBy).append(", lasModTs=").append(lasModTs).append(", insertedBy=")
				.append(insertedBy).append(", categoryId=").append(categoryId).append(", subCategoryId=")
				.append(subCategoryId).append(", subCategoryName=").append(subCategoryName).append(", insertedDate=")
				.append(insertedDate).append("]").toString();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SubCategoryId", nullable = false, unique = true)
	public Integer getSubCategoryId() {
		return subCategoryId;
	}
	@Column(name = "LastModBy", length = 50, nullable = false)
	public String getLasModBy() {
		return lasModBy;
	}
	@Column(name = "LastModTs", nullable = false)
	public String getLasModTs() {
		return lasModTs;
	}
	@Column(name = "InsertedBy", length = 50, nullable = false)
	public String getInsertedBy() {
		return insertedBy;
	}
	@Column(name = "CategoryId", nullable = false)
	public Integer getCategoryId() {
		return categoryId;
	}
	@Column(name = "SubCategoryName", length = 25, nullable = false)
	public String getSubCategoryName() {
		return subCategoryName;
	}
	@Column(name = "InsertedDate", nullable = false)
	public LocalDate getInsertedDate() {
		return insertedDate;
	}
	public SubCategoriesTable setLasModBy(String lasModBy) {
		this.lasModBy = lasModBy;
		return this;
	}
	public SubCategoriesTable setLasModTs(String lasModTs) {
		this.lasModTs = lasModTs;
		return this;
	}
	public SubCategoriesTable setInsertedBy(String insertedBy) {
		this.insertedBy = insertedBy;
		return this;
	}
	public SubCategoriesTable setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
		return this;
	}
	public SubCategoriesTable setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
		return this;
	}
	public SubCategoriesTable setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
		return this;
	}
	public SubCategoriesTable setInsertedDate(LocalDate insertedDate) {
		this.insertedDate = insertedDate;
		return this;
	}
}